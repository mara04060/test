<?php

namespace AppBundle\Controller;
use AppBundle\AppBundle;
use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends Controller
{
    /**
     * @Route("/category/", name="category_list")
     */
 public function indexAction()
 {
     $title = "Full list Category Page";
     $category = $this->getDoctrine()
         ->getRepository('AppBundle:Category')
         ->findBy(['active'=>true], ['name' => 'DESC']);

     return $this->render('@App/category/index.html.twig',
         [
             'title'=>$title ,
             'category'=>$category,
         ]);
 }
    /**
     * @Route("/category/{id}/", name="category_item", requirements={"id":"[0-9]+"} )
     * @param Category $category
     * @return array
     */
    public function showProductInCategory(Category $category)
    {
        $title = "Only One Category";
        $product=$this->getDoctrine()->getRepository('AppBundle:Product')
            ->productInCategory($category);
        if(!$product)
        {
            throw $this->createNotFoundException('Product not Found');
        }
        return $this->render('@App/category/category.html.twig',
            [
                'title'=>$title,
                'in_category'=>$product,
            ]);
    }

}