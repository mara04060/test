<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $title = "Start Page";

        return $this->render('@App/default/index.html.twig', [
            'title'=>$title,
        ]);
    }
    /**
     * @Route("/feedback/", name="feedback")
     */
    public function feedbackAction()
    {
        $title = "feedback Page";
        return $this->render('@App/default/feedback.html.twig', [
            'title'=>$title,
        ]);
    }
}
