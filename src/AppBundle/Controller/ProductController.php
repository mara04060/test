<?php

namespace AppBundle\Controller;
use AppBundle\AppBundle;
use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends Controller
{
    /**
     * @Route("/products/", name="products_list")
     */
 public function indexAction()
 {
     $title = "Full list Product Page";
     $product = $this->getDoctrine()->getRepository('AppBundle:Product')->findActive();
     return $this->render('@App/products/index.html.twig',
         [
             'title'=>$title ,
             'product'=>$product,
         ]);
 }

    /**
     * @Route("/products/{id}/", name="product_item", requirements={"id":"[0-9]+"} )
     * @param $id
     * @return array
     */
 public function showAction(Product $product)
 {
     $title = "Only One Product";
//     $product=$this->getDoctrine()->getRepository('AppBundle:Product')->find($id);
//     if(!$product)
//     {
//         throw $this->createNotFoundException('Product not Found');
//     }
     return $this->render('@App/products/product.html.twig',
         [
             'title'=>$title,
             'product'=>$product,
         ]);
 }

}